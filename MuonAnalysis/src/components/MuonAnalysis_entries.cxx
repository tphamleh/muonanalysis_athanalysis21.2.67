
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../MuonAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( MuonAnalysisAlg )

DECLARE_FACTORY_ENTRIES( MuonAnalysis ) 
{
  DECLARE_ALGORITHM( MuonAnalysisAlg );
}
