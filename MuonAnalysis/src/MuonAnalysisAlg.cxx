// MuonAnalysis includes
#include "MuonAnalysisAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
//#include <HIEventUtils/IHICentralityTool.h>
//#include <HIEventUtils/HICentralityTool.h>
#include "xAODHIEvent/HIEventShape.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackingPrimitives.h"




MuonAnalysisAlg::MuonAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


MuonAnalysisAlg::~MuonAnalysisAlg() {}


StatusCode MuonAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory
  m_muonHist = new TH1D("muonHist","muonHist",100,0,100);
  CHECK( histSvc()->regHist("/MYSTREAM/muonHist", m_muonHist) ); //registers histogram to output stream

  m_muonTree = new TTree("muonTree","muonTree");
  m_muonTree->Branch("runNumber",   &m_runNumber);
  m_muonTree->Branch("eventNumber",   &m_eventNumber);
  m_muonTree->Branch("lumiBlock",   &m_lumiBlock);

  // muon kinematics
  m_muonTree->Branch("muon_pt",    &m_muon_pt);
  m_muonTree->Branch("muon_eta",    &m_muon_eta);
  m_muonTree->Branch("muon_phi",    &m_muon_phi);
  m_muonTree->Branch("muon_charge",    &m_muon_charge);
  m_muonTree->Branch("muon_quality",    &m_muon_quality);
  m_muonTree->Branch("muon_type",    &m_muon_type);

  // L1 muon flags
  m_muonTree->Branch("L1_MU4",    &m_L1_MU4);
  m_muonTree->Branch("L1_MU6",    &m_L1_MU6);
  m_muonTree->Branch("L1_MU11",    &m_L1_MU11);
  m_muonTree->Branch("L1_MU15",    &m_L1_MU15);
  m_muonTree->Branch("L1_MU20",    &m_L1_MU20);

  m_muonTree->Branch("match_L1_MU4",    &m_match_L1_MU4);
  m_muonTree->Branch("match_L1_MU6",    &m_match_L1_MU6);
  m_muonTree->Branch("match_L1_MU11",    &m_match_L1_MU11);
  m_muonTree->Branch("match_L1_MU15",    &m_match_L1_MU15);
  m_muonTree->Branch("match_L1_MU20",    &m_match_L1_MU20);

  // HLT muon flags
  m_muonTree->Branch("HLT_mu3",    &m_HLT_mu3);
  m_muonTree->Branch("HLT_mu4",    &m_HLT_mu4);
  m_muonTree->Branch("HLT_mu6",    &m_HLT_mu6);
  m_muonTree->Branch("HLT_mu8",    &m_HLT_mu8);
  m_muonTree->Branch("HLT_mu10",    &m_HLT_mu10);
  m_muonTree->Branch("HLT_mu14",    &m_HLT_mu14);
  m_muonTree->Branch("HLT_mu15",    &m_HLT_mu15);

  m_muonTree->Branch("match_HLT_mu3",    &m_match_HLT_mu3);
  m_muonTree->Branch("match_HLT_mu4",    &m_match_HLT_mu4);
  m_muonTree->Branch("match_HLT_mu6",    &m_match_HLT_mu6);
  m_muonTree->Branch("match_HLT_mu8",    &m_match_HLT_mu8);
  m_muonTree->Branch("match_HLT_mu10",    &m_match_HLT_mu10);
  m_muonTree->Branch("match_HLT_mu14",    &m_match_HLT_mu14);
  m_muonTree->Branch("match_HLT_mu15",    &m_match_HLT_mu15);
  m_muonTree->Branch("match_HLT_mu15_L1MU4",    &m_match_HLT_mu15_L1MU4);
  m_muonTree->Branch("match_HLT_mu15_L1MU6",    &m_match_HLT_mu15_L1MU6);
  m_muonTree->Branch("match_HLT_mu15_L1MU8",    &m_match_HLT_mu15_L1MU8);
  m_muonTree->Branch("match_HLT_mu15_L1MU10",    &m_match_HLT_mu15_L1MU10);

  // muon tracks
  m_muonTree->Branch("IDtrackLink_pt", &m_IDtrackLink_pt);
  m_muonTree->Branch("MStrackLink_pt", &m_MStrackLink_pt);
  m_muonTree->Branch("CBtrackLink_pt", &m_CBtrackLink_pt);
  m_muonTree->Branch("IDtrackLink_phi", &m_IDtrackLink_phi);
  m_muonTree->Branch("MStrackLink_phi", &m_MStrackLink_phi);
  m_muonTree->Branch("CBtrackLink_phi", &m_CBtrackLink_phi);
  m_muonTree->Branch("IDtrackLink_eta", &m_IDtrackLink_eta);
  m_muonTree->Branch("MStrackLink_eta", &m_MStrackLink_eta);
  m_muonTree->Branch("CBtrackLink_eta", &m_CBtrackLink_eta);
  m_muonTree->Branch("IDtrackLink_d0", &m_IDtrackLink_d0);
  m_muonTree->Branch("IDtrackLink_d0err", &m_IDtrackLink_d0err);
  m_muonTree->Branch("IDtrackLink_z0", &m_IDtrackLink_z0);
  m_muonTree->Branch("IDtrackLink_z0err", &m_IDtrackLink_z0err);
  m_muonTree->Branch("IDtrackLink_d0sig", &m_IDtrackLink_d0sig);
  m_muonTree->Branch("IDtrackLink_z0sig", &m_IDtrackLink_z0sig);
  m_muonTree->Branch("MStrackLink_d0", &m_MStrackLink_d0);
  m_muonTree->Branch("MStrackLink_d0err", &m_MStrackLink_d0err);
  m_muonTree->Branch("MStrackLink_z0", &m_MStrackLink_z0);
  m_muonTree->Branch("MStrackLink_z0err", &m_MStrackLink_z0err);
  m_muonTree->Branch("MStrackLink_d0sig", &m_MStrackLink_d0sig);
  m_muonTree->Branch("MStrackLink_z0sig", &m_MStrackLink_z0sig);
  m_muonTree->Branch("CBtrackLink_d0", &m_CBtrackLink_d0);
  m_muonTree->Branch("CBtrackLink_d0err", &m_CBtrackLink_d0err);
  m_muonTree->Branch("CBtrackLink_z0", &m_CBtrackLink_z0);
  m_muonTree->Branch("CBtrackLink_z0err", &m_CBtrackLink_z0err);
  m_muonTree->Branch("CBtrackLink_d0sig", &m_CBtrackLink_d0sig);
  m_muonTree->Branch("CBtrackLink_z0sig", &m_CBtrackLink_z0sig);

  // FCal Energy
  m_muonTree->Branch("FCalSumEt",    &m_FCalSumEt);

  m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
  CHECK( m_tdt.initialize() );

  m_tmt.setTypeAndName("Trig::MatchingTool/MyMatchingTool");
  CHECK( m_tmt.initialize() );

  m_muonSelection.setTypeAndName("CP::MuonSelectionTool/MyMuonSelectionTool");
  CHECK( m_muonSelection.initialize() );

  CHECK( histSvc()->regTree("/MYSTREAM/muonTree", m_muonTree) ); //registers tree to output stream inside a sub-directory


  return StatusCode::SUCCESS;
}

StatusCode MuonAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode MuonAnalysisAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed



  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  m_runNumber = ei->runNumber();
  m_eventNumber = ei->eventNumber();
  m_lumiBlock = ei->lumiBlock();

  // L1 muon
  m_L1_MU4 = m_tdt->isPassed("L1_MU4");
  m_L1_MU6 = m_tdt->isPassed("L1_MU6");
  m_L1_MU11 = m_tdt->isPassed("L1_MU11");
  m_L1_MU15 = m_tdt->isPassed("L1_MU15");
  m_L1_MU20 = m_tdt->isPassed("L1_MU20");
  // HLT muon
  m_HLT_mu3 = m_tdt->isPassed("HLT_mu3");
  m_HLT_mu4 = m_tdt->isPassed("HLT_mu4");
  m_HLT_mu6 = m_tdt->isPassed("HLT_mu6");
  m_HLT_mu8 = m_tdt->isPassed("HLT_mu8");
  m_HLT_mu10 = m_tdt->isPassed("HLT_mu10");
  m_HLT_mu14 = m_tdt->isPassed("HLT_mu14");
  m_HLT_mu15 = m_tdt->isPassed("HLT_mu15");
  m_HLT_mu15_L1MU10 = m_tdt->isPassed("HLT_mu15_L1MU10");
  m_HLT_mu15_L1MU6 = m_tdt->isPassed("HLT_mu15_L1MU6");
  m_HLT_mu15_L1MU4 = m_tdt->isPassed("HLT_mu15_L1MU4");
  m_HLT_mu15_L1MU8 = m_tdt->isPassed("HLT_mu15_L1MU8");

  // loop over muons and save them in vectors
  const xAOD::MuonContainer* muons = 0;
  CHECK( evtStore()->retrieve(muons,"CalibratedMuons") );

  const xAOD::MuonRoIContainer* muonrois = 0;
  CHECK( evtStore()->retrieve(muonrois, "LVL1MuonRoIs") );

  for(const auto *muon_itr: *muons) {
     //ATH_MSG_INFO( "  muon pt = " << muon_itr->pt()/1000. );
     //if(muon_itr->pt()/1000.<20.) continue;
     //if(fabs(muon_itr->eta())>2.5) continue;
     m_muon_pt->push_back(muon_itr->pt()/1000);
     m_muon_eta->push_back(muon_itr->eta());
     m_muon_phi->push_back(muon_itr->phi());
     m_muon_charge->push_back(muon_itr->charge());
     m_muon_type->push_back(muon_itr->muonType());
     xAOD::Muon::Quality my_quality;
     my_quality = m_muonSelection->getQuality(*muon_itr);
     m_muon_quality->push_back(my_quality);

     // Tracks from ElementLink
   	const xAOD::TrackParticle* idtp =  0;
   	const xAOD::TrackParticle* mstp = 0;
   	const xAOD::TrackParticle* cbtp = 0;
    ElementLink<xAOD::TrackParticleContainer> idtpLink = (muon_itr)->inDetTrackParticleLink();
   	ElementLink<xAOD::TrackParticleContainer> mstpLink = (muon_itr)->extrapolatedMuonSpectrometerTrackParticleLink();
   	ElementLink<xAOD::TrackParticleContainer> cbtpLink = (muon_itr)->combinedTrackParticleLink();
    if(idtpLink.isValid()) {
       	idtp = *idtpLink;
     		m_IDtrackLink_pt->push_back(idtp->pt()/1000.);
     		m_IDtrackLink_eta->push_back(idtp->eta());
     		m_IDtrackLink_phi->push_back(idtp->phi());
     		m_IDtrackLink_d0->push_back(idtp->d0());
     		m_IDtrackLink_z0->push_back(idtp->z0());
     		m_IDtrackLink_d0err->push_back(sqrt(idtp->definingParametersCovMatrix()(0,0)));
     		m_IDtrackLink_z0err->push_back(sqrt(idtp->definingParametersCovMatrix()(1,1)));
     		m_IDtrackLink_d0sig->push_back(fabs(idtp->d0())/sqrt(idtp->definingParametersCovMatrix()(0,0)));
     		m_IDtrackLink_z0sig->push_back(fabs(idtp->z0())/sqrt(idtp->definingParametersCovMatrix()(1,1)));
   		}
   	if(mstpLink.isValid()) {
     		mstp = *mstpLink;
     		m_MStrackLink_pt->push_back(mstp->pt()/1000.);
     		m_MStrackLink_eta->push_back(mstp->eta());
     		m_MStrackLink_phi->push_back(mstp->phi());
     		m_MStrackLink_d0->push_back(mstp->d0());
     		m_MStrackLink_z0->push_back(mstp->z0());
     		m_MStrackLink_d0err->push_back(sqrt(mstp->definingParametersCovMatrix()(0,0)));
     		m_MStrackLink_z0err->push_back(sqrt(mstp->definingParametersCovMatrix()(1,1)));
     		m_MStrackLink_d0sig->push_back(fabs(mstp->d0())/sqrt(mstp->definingParametersCovMatrix()(0,0)));
     		m_MStrackLink_z0sig->push_back(fabs(mstp->z0())/sqrt(mstp->definingParametersCovMatrix()(1,1)));
   		}
   	if(cbtpLink.isValid()) {
       	cbtp = *cbtpLink;
     		m_CBtrackLink_pt->push_back(cbtp->pt()/1000.);
     		m_CBtrackLink_eta->push_back(cbtp->eta());
     		m_CBtrackLink_phi->push_back(cbtp->phi());
     		m_CBtrackLink_d0->push_back(cbtp->d0());
     		m_CBtrackLink_z0->push_back(cbtp->z0());
     		m_CBtrackLink_d0err->push_back(sqrt(cbtp->definingParametersCovMatrix()(0,0)));
     		m_CBtrackLink_z0err->push_back(sqrt(cbtp->definingParametersCovMatrix()(1,1)));
     		m_CBtrackLink_d0sig->push_back(fabs(cbtp->d0())/sqrt(cbtp->definingParametersCovMatrix()(0,0)));
     		m_CBtrackLink_z0sig->push_back(fabs(cbtp->z0())/sqrt(cbtp->definingParametersCovMatrix()(1,1)));
   		}

      m_match_HLT_mu3->push_back( m_tmt->match( *muon_itr, "HLT_mu3", 0.02, false) );
     	m_match_HLT_mu4->push_back( m_tmt->match( *muon_itr, "HLT_mu4", 0.02, false) );
      m_match_HLT_mu6->push_back( m_tmt->match( *muon_itr, "HLT_mu6", 0.02, false) );
     	m_match_HLT_mu8->push_back( m_tmt->match( *muon_itr, "HLT_mu8", 0.1, false) );
    	m_match_HLT_mu10->push_back( m_tmt->match( *muon_itr, "HLT_mu10", 0.1, false) );
    	m_match_HLT_mu14->push_back( m_tmt->match( *muon_itr, "HLT_mu14", 0.1, false) );
      m_match_HLT_mu15->push_back( m_tmt->match( *muon_itr, "HLT_mu15", 0.1, false) );
      m_match_HLT_mu15_L1MU4->push_back( m_tmt->match( *muon_itr, "HLT_mu15_L1MU4", 0.1, false) );
      m_match_HLT_mu15_L1MU6->push_back( m_tmt->match( *muon_itr, "HLT_mu15_L1MU6", 0.1, false) );
      m_match_HLT_mu15_L1MU8->push_back( m_tmt->match( *muon_itr, "HLT_mu15_L1MU8", 0.1, false) );
      m_match_HLT_mu15_L1MU10->push_back( m_tmt->match( *muon_itr, "HLT_mu15_L1MU10", 0.1, false) );

      // match to L1 ROIs by hand
    	double l1dr = 0.5;
    	double min_mu4, min_mu6, min_mu11, min_mu15, min_mu20;
      min_mu4 = l1dr; min_mu6 = l1dr; min_mu11 = l1dr; min_mu15 = l1dr; min_mu20 = l1dr;
    	for(const auto *l1muon_itr: *muonrois) {
    		if(l1muon_itr->thrValue()>=4000) {
          double dR1 = get_dR(muon_itr->eta(), muon_itr->phi(), l1muon_itr->eta(), l1muon_itr->phi());
          if(dR1<min_mu4) min_mu4=dR1;
        }
    		if(l1muon_itr->thrValue()>=6000) {
          double dR2 = get_dR(muon_itr->eta(), muon_itr->phi(), l1muon_itr->eta(), l1muon_itr->phi());
          if(dR2<min_mu6) min_mu6=dR2;
        }
    		if(l1muon_itr->thrValue()>=11000) {
          double dR3 = get_dR(muon_itr->eta(), muon_itr->phi(), l1muon_itr->eta(), l1muon_itr->phi());
          if(dR3<min_mu11) min_mu11=dR3;
        }
        if(l1muon_itr->thrValue()>=15000) {
          double dR4 = get_dR(muon_itr->eta(), muon_itr->phi(), l1muon_itr->eta(), l1muon_itr->phi());
          if(dR4<min_mu15) min_mu15=dR4;
        }
        if(l1muon_itr->thrValue()>=20000) {
          double dR5 = get_dR(muon_itr->eta(), muon_itr->phi(), l1muon_itr->eta(), l1muon_itr->phi());
          if(dR5<min_mu20) min_mu20=dR5;
        }
	  	}

    	// match to the trigger
       	m_match_L1_MU4->push_back( min_mu4<l1dr );
       	m_match_L1_MU6->push_back( min_mu6<l1dr );
       	m_match_L1_MU11->push_back( min_mu11<l1dr );
        m_match_L1_MU15->push_back( min_mu15<l1dr );
        m_match_L1_MU20->push_back( min_mu20<l1dr );


   }


   m_FCalSumEt = ReturnFCalEnergy();
  //m_muonHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram

  m_muonTree->Fill();
  m_muon_pt->clear();
  m_muon_eta->clear();
  m_muon_phi->clear();
  m_muon_charge->clear();
  m_muon_quality->clear();
  m_muon_type->clear();

  m_IDtrackLink_pt->clear();
  m_MStrackLink_pt->clear();
  m_CBtrackLink_pt->clear();
  m_IDtrackLink_phi->clear();
  m_MStrackLink_phi->clear();
  m_CBtrackLink_phi->clear();
  m_IDtrackLink_eta->clear();
  m_MStrackLink_eta->clear();
  m_CBtrackLink_eta->clear();
  m_IDtrackLink_d0->clear();
  m_IDtrackLink_z0->clear();
  m_IDtrackLink_d0err->clear();
  m_IDtrackLink_z0err->clear();
  m_IDtrackLink_d0sig->clear();
  m_IDtrackLink_z0sig->clear();
  m_MStrackLink_d0->clear();
  m_MStrackLink_z0->clear();
  m_MStrackLink_d0err->clear();
  m_MStrackLink_z0err->clear();
  m_MStrackLink_d0sig->clear();
  m_MStrackLink_z0sig->clear();
  m_CBtrackLink_d0->clear();
  m_CBtrackLink_z0->clear();
  m_CBtrackLink_d0err->clear();
  m_CBtrackLink_z0err->clear();
  m_CBtrackLink_d0sig->clear();
  m_CBtrackLink_z0sig->clear();

  m_match_L1_MU4->clear();
  m_match_L1_MU6->clear();
  m_match_L1_MU11->clear();
  m_match_L1_MU15->clear();
  m_match_L1_MU20->clear();
  m_match_HLT_mu3->clear();
  m_match_HLT_mu4->clear();
  m_match_HLT_mu6->clear();
  m_match_HLT_mu8->clear();
  m_match_HLT_mu10->clear();
  m_match_HLT_mu14->clear();
  m_match_HLT_mu15->clear();
  m_match_HLT_mu15_L1MU4->clear();
  m_match_HLT_mu15_L1MU6->clear();
  m_match_HLT_mu15_L1MU8->clear();
  m_match_HLT_mu15_L1MU10->clear();


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode MuonAnalysisAlg::beginInputFile() {
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
   //const xAOD::CutBookkeeperContainer* bks = 0;
   //CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}

double MuonAnalysisAlg::get_dR(const double eta1, const double phi1, const double eta2, const double phi2) {
    double deta = fabs(eta1 - eta2);
    double dphi = fabs(phi1 - phi2) < TMath::Pi() ? fabs(phi1 - phi2) : 2*TMath:: \
      Pi() - fabs(phi1 - phi2);
    return sqrt(deta*deta + dphi*dphi);
  }

double MuonAnalysisAlg::ReturnFCalEnergy() {
      /*const xAOD::VertexContainer * vertices = 0;
      ATH_CHECK( evtStore()->retrieve (vertices, "PrimaryVertices"));
      xAOD::VertexContainer::const_iterator vtx_itr = vertices->begin();
      xAOD::VertexContainer::const_iterator vtx_end = vertices->end();
      const xAOD::Vertex* pileup = 0;
      int pile_up_vertices=0;
      int trackn = 0;
      for(;vtx_itr!=vtx_end;++vtx_itr) {
            if((*vtx_itr)->vertexType()==xAOD::VxType::PileUp) {
                  pileup = (*vtx_itr);
                  trackn = pileup->nTrackParticles();
                  if(trackn>6) pile_up_vertices=1;
              }
         }//if the number of tracks>6 then this is a pile up vertex
     auto centTool = make_unique<HI::HICentralityTool>("CentralityTool");
     ANA_CHECK( centTool->setProperty("RunSpecies","pPb2016") );
     ANA_CHECK( centTool->initialize() );
  	double ETsumFCal = 0;
    ETsumFCal = centTool->getCentralityEstimator();
    return ETsumFCal;
  	float Centrality = 0;
  	if (!pile_up_vertices) Centrality = centTool->getCentralityPercentile();*/

	  const xAOD::HIEventShapeContainer *hiue(0);
    CHECK( evtStore()->retrieve(hiue,"CaloSums") );
	  xAOD::HIEventShapeContainer::const_iterator es_itr = hiue->begin();
	  xAOD::HIEventShapeContainer::const_iterator es_end = hiue->end();
	  double m_fcalEt = 0;
	  for (;es_itr!=es_end;es_itr++ ) {
	      float et = (*es_itr)->et();
	      const std::string name = (*es_itr)->auxdataConst<std::string>("Summary");
	      if (name=="FCal") m_fcalEt = et*1e-6;
	    }
	  return m_fcalEt;
	}
