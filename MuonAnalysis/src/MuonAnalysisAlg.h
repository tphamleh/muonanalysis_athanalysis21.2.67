#ifndef MUONANALYSIS_MUONANALYSISALG_H
#define MUONANALYSIS_MUONANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "xAODHIEvent/HIEventShapeContainer.h"
//#include <HIEventUtils/IHICentralityTool.h>
//#include <HIEventUtils/HICentralityTool.h>
#include "xAODHIEvent/HIEventShape.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
//Example ROOT Includes
#include "TTree.h"
#include "TH1D.h"



class MuonAnalysisAlg: public ::AthAnalysisAlgorithm {
 public:
  MuonAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MuonAnalysisAlg();

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  //virtual StatusCode evtStore();


  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp



 private:

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;
   float m_runNumber;
   float m_eventNumber;
   float m_lumiBlock;
   // L1 muon
   bool m_L1_MU4;
   bool m_L1_MU6;
   bool m_L1_MU11;
   bool m_L1_MU15;
   bool m_L1_MU20;
   // HLT muon
   bool m_HLT_mu3;
   bool m_HLT_mu4;
   bool m_HLT_mu6;
   bool m_HLT_mu8;
   bool m_HLT_mu10;
   bool m_HLT_mu14;
   bool m_HLT_mu15;
   bool m_HLT_mu15_L1MU4;
   bool m_HLT_mu15_L1MU6;
   bool m_HLT_mu15_L1MU8;
   bool m_HLT_mu15_L1MU10;

   float m_FCalSumEt;

   // muon variables
   std::vector<float> *m_muon_pt=0;
   std::vector<float> *m_muon_eta=0;
   std::vector<float> *m_muon_phi=0;
   std::vector<float> *m_muon_charge=0;
   std::vector<unsigned int> *m_muon_quality=0;
   std::vector<unsigned int> *m_muon_type=0;

   std::vector<bool> *m_match_L1_MU4=0;
   std::vector<bool> *m_match_L1_MU6=0;
   std::vector<bool> *m_match_L1_MU11=0; // HLT_noalg_eb_L1MU4
   std::vector<bool> *m_match_L1_MU15=0;
   std::vector<bool> *m_match_L1_MU20=0;
   std::vector<bool> *m_match_HLT_mu3=0;
   std::vector<bool> *m_match_HLT_mu4=0;
   std::vector<bool> *m_match_HLT_mu6=0;
   std::vector<bool> *m_match_HLT_mu8=0;
   std::vector<bool> *m_match_HLT_mu10=0;
   std::vector<bool> *m_match_HLT_mu14=0;
   std::vector<bool> *m_match_HLT_mu15=0;
   std::vector<bool> *m_match_HLT_mu15_L1MU4=0;
   std::vector<bool> *m_match_HLT_mu15_L1MU6=0;
   std::vector<bool> *m_match_HLT_mu15_L1MU8=0;
   std::vector<bool> *m_match_HLT_mu15_L1MU10=0;

   std::vector<float> *m_IDtrackLink_pt=0;
   std::vector<float> *m_MStrackLink_pt=0;
   std::vector<float> *m_CBtrackLink_pt=0;
   std::vector<float> *m_IDtrackLink_phi=0;
   std::vector<float> *m_MStrackLink_phi=0;
   std::vector<float> *m_CBtrackLink_phi=0;
   std::vector<float> *m_IDtrackLink_eta=0;
   std::vector<float> *m_MStrackLink_eta=0;
   std::vector<float> *m_CBtrackLink_eta=0;
   std::vector<float> *m_IDtrackLink_d0=0;
   std::vector<float> *m_IDtrackLink_z0=0;
   std::vector<float> *m_IDtrackLink_d0err=0;
   std::vector<float> *m_IDtrackLink_z0err=0;
   std::vector<float> *m_IDtrackLink_d0sig=0;
   std::vector<float> *m_IDtrackLink_z0sig=0;
   std::vector<float> *m_MStrackLink_d0=0;
   std::vector<float> *m_MStrackLink_z0=0;
   std::vector<float> *m_MStrackLink_d0err=0;
   std::vector<float> *m_MStrackLink_z0err=0;
   std::vector<float> *m_MStrackLink_d0sig=0;
   std::vector<float> *m_MStrackLink_z0sig=0;
   std::vector<float> *m_CBtrackLink_d0=0;
   std::vector<float> *m_CBtrackLink_z0=0;
   std::vector<float> *m_CBtrackLink_d0err=0;
   std::vector<float> *m_CBtrackLink_z0err=0;
   std::vector<float> *m_CBtrackLink_d0sig=0;
   std::vector<float> *m_CBtrackLink_z0sig=0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   //TTree* m_myTree = 0;
   TH1D* m_muonHist = 0;
   TTree* m_muonTree = 0;

   asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;
   asg::AnaToolHandle<Trig::MatchingTool > m_tmt;
   asg::AnaToolHandle<CP::MuonSelectionTool > m_muonSelection;

   double get_dR(const double eta1, const double phi1, const double eta2, const double phi2);
   double ReturnFCalEnergy();

};

#endif //> !MUONANALYSIS_MUONANALYSISALG_H
